//
//  PatentDetailViewController.swift
//  NASA
//
//  Created by DONALD CHWOJKO on 8/9/17.
//  Copyright © 2017 DONALD CHWOJKO. All rights reserved.
//

import UIKit

class PatentDetailViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    
    var word: String! {
        didSet (newWord) {
            self.refreshUI()
        }
    }
    
    func refreshUI() {
        label?.text = word
//        descriptionLabel?.text = monster.description
//        iconImageView?.image = UIImage(named: monster.iconName)
//        weaponImageView?.image = monster.weaponImage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        refreshUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PatentDetailViewController: WordSelectionDelegate {
    func wordSelected(newWord: String) {
        word = newWord
    }
}
