//
//  Patent.swift
//  NASA
//
//  Created by DONALD CHWOJKO on 8/9/17.
//  Copyright © 2017 DONALD CHWOJKO. All rights reserved.
//

import Foundation

class Patent: NSObject {
    var category: String?
    var client_record_id: String?
    var center: String?
    var eRelations: [AnyObject]?
    var reference_number: String?
    var expiration_date: String?
    var abstract: String?
    var title: String?
    var innovator: [[String:String]]?
    var contact: [String:String]?
    var publication: String?
    var concepts: [String:String]?
    var serial_number: String?
    var _id: String?
    var patent_number: String?
    var id: String?
    var trl: String?
    
    init(category: String?) {
        self.category = category
        super.init()
    }
}
