//
//  APODDetailViewController.swift
//  NASA
//
//  Created by DONALD CHWOJKO on 8/8/17.
//  Copyright © 2017 DONALD CHWOJKO. All rights reserved.
//

import UIKit

class APODDetailViewController: UIViewController {

    @IBOutlet weak var datepicker: UIDatePicker!
    @IBOutlet weak var imageview: UIImageView!
    
    @IBOutlet weak var imageViewWindow: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var apodTitle: UILabel!
    
    @IBOutlet weak var explanation: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // TAP GESTURES
        
        DispatchQueue.main.async {
            self.descriptionView.isHidden = true
            self.view.bringSubview(toFront: self.descriptionView)
            self.view.bringSubview(toFront: self.imageview)
            self.view.bringSubview(toFront: self.activityIndicator)
        }
        
        imageview.backgroundColor = UIColor.black
        
        self.activityIndicator.isHidden = true
        
        datepicker.datePickerMode = UIDatePickerMode.date
        datepicker.maximumDate = Date()
        
        
        
        datepicker.addTarget(self, action: #selector(APODDetailViewController.dateChanged(datepicker:)), for: .valueChanged)
        dateChanged(datepicker: datepicker)
    }
    
    func handleImageTap(sender: UITapGestureRecognizer) {
        // TO DO: imageview to back, description view to front
        
        print("handleImageTap")
            self.imageview.isHidden = true
        self.descriptionView.isHidden = false
            //self.view.bringSubview(toFront: self.descriptionView)
            //self.view.bringSubview(toFront: self.activityIndicator)
    }
    
    func handleDescriptionTap(sender: UITapGestureRecognizer) {
        // TO DO: imageview to front, description view to back
print("handleDescriptionTap")
        self.imageview.isHidden = false
        self.descriptionView.isHidden = true
            //self.view.bringSubview(toFront: self.imageview)
            //self.view.bringSubview(toFront: self.activityIndicator)

    }
    
    func dateChanged(datepicker: UIDatePicker) {
        DispatchQueue.main.async {
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"//"MM/dd/yy"
        let dateString = dateFormatter.string(from: datepicker.date)
        print(dateString)
        NASAClient.sharedInstance().getAstronomyPictureOfTheDay(dateString: dateString) { (apod, error) in
            guard error == nil else {
                print("there was an error")
                return
            }
            
            guard apod != nil else {
                print("apod is nil")
                return
            }
            
            var explanationText: String?
            if let explanation = apod?.explanation {
                explanationText = explanation
            } else {
                explanationText = "No description available"
            }
            
            DispatchQueue.main.async {
                self.explanation.text = explanationText
            }
            
            var titleText: String?
            if let t = apod?.imageTitle {
                titleText = t
            } else {
                titleText = "No title available"
            }
            DispatchQueue.main.async {
                self.apodTitle.text = titleText
            }
            
            if let url = apod?.hdUrl {
                NASAClient.sharedInstance().getImage(urlString: url) { (data, error) in
                    guard error == nil else {
                        print("there was an error")
                        let image = UIImage(named: "Placeholder")
                        DispatchQueue.main.async {
                            self.imageview.image = image
                            self.activityIndicator.stopAnimating()
                            self.activityIndicator.isHidden = true
                        }
                        return
                    }
                    
                    guard data != nil else {
                        print("data was nil")
                        return
                    }
                    
                    let image = UIImage(data: data as! Data)
                    DispatchQueue.main.async {
                        self.imageview.image = image
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.isHidden = true
                    }
                }
            } else if let url = apod?.url {
                NASAClient.sharedInstance().getImage(urlString: url) { (data, error) in
                    guard error == nil else {
                        print("there was an error")
                        let image = UIImage(named: "Placeholder")
                        DispatchQueue.main.async {
                            self.imageview.image = image
                            self.activityIndicator.stopAnimating()
                            self.activityIndicator.isHidden = true
                        }
                        return
                    }
                    
                    guard data != nil else {
                        print("data was nil")
                        return
                    }
                    
                    let image = UIImage(data: data as! Data)
                    DispatchQueue.main.async {
                        self.imageview.image = image
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.isHidden = true
                    }
                }
            } else {
                print("no url")
                let image = UIImage(named: "Placeholder")
                DispatchQueue.main.async {
                    self.imageview.image = image
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
