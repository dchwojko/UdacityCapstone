//
//  NASAConstants.swift
//  NASA
//
//  Created by DONALD CHWOJKO on 8/7/17.
//  Copyright © 2017 DONALD CHWOJKO. All rights reserved.
//

import Foundation

extension NASAClient {
    struct Constants {
        static let ApiScheme = "https"
        static let ApiHost = "api.nasa.gov"
        static let ApiPath_PlanetaryApod = "/planetary/apod"
        static let ApiPath_NEO = "/neo/rest/v1"
        static let ApiPath_Patents = "/patents/content"
        static let ApiPersonalKey = "PBheEcv71QgF02rnKweH3PNuMfOGxyM0RsObQmXF"
    }
    
    struct ParameterKeys {
        static let APIKEY = "api_key"
        
        struct APOD {
            static let HD = "hd"
            static let DATE = "date"
            static let START_DATE = "start_date"
            static let END_DATE = "end_date"
        }
        
        struct Patents {
            static let QUERY = "query"
            static let CONCEPT_TAGS = "concept_tags"
            static let LIMIT = "limit"
        }
    }
    
    struct Methods {
        static let NEO_FEED = "/feed"
        static let NEO_LOOKUP = "/neo/{id}"
        static let NEO_BROWSE = "/neo/browse"
/*        static let GET_FACILITIES = "/facilities"
        static let GET_FACILITY = "/facilities/{facilityID}"
        static let GET_FACILITIES_ADDRESS = "/facilityaddresses"
        static let GET_FACILITY_ADDRESS = "/facilities/{facilityID}/facilityaddresses"
        static let GET_FACILITY_CAMPSITES = "/facilities/{facilityID}/campsites"*/
    }
}
