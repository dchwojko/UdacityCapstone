//
//  NASAConvenience.swift
//  NASA
//
//  Created by DONALD CHWOJKO on 8/7/17.
//  Copyright © 2017 DONALD CHWOJKO. All rights reserved.
//

import Foundation

extension NASAClient {
    func getAstronomyPictureOfTheDay(dateString: String?, completionHandler: @escaping (_ apod: APOD?, _ error: NSError?) -> Void) {
        
        var components = URLComponents()
        components.scheme = NASAClient.Constants.ApiScheme
        components.host = NASAClient.Constants.ApiHost
        components.path = NASAClient.Constants.ApiPath_PlanetaryApod
        let queryItem1 = URLQueryItem(name: NASAClient.ParameterKeys.APIKEY, value: NASAClient.Constants.ApiPersonalKey)
        let queryItem2 = URLQueryItem(name: NASAClient.ParameterKeys.APOD.HD, value: "true")
        if dateString == nil {
            components.queryItems = [queryItem1, queryItem2]
        } else {
            let queryItem3 = URLQueryItem(name: NASAClient.ParameterKeys.APOD.DATE, value: dateString)
            components.queryItems = [queryItem1, queryItem2, queryItem3]
        }
        
        NASAClient.sharedInstance().taskForGETMethod(url: components.url!) { (data, error) in
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            
            guard data != nil else {
                completionHandler(nil, nil)
                return
            }
            
            // MUST HAVE THE DATA
            if let data = data {
                let date: String! = data["date"]! as! String
                let explanation: String? = data["explanation"]! as? String
                let hdUrl: String? = data["hdurl"]! as? String
                let imageTitle: String? = data["title"]! as? String
                let url: String? = data["url"]! as? String
                let apod = APOD(date: date, explanation: explanation, hdUrl: hdUrl, imageTitle: imageTitle, url: url)
                completionHandler(apod, nil)
                return
            } else {
                completionHandler(nil, nil)
                return
            }
        }
    }
    
    func getImage(urlString: String, completionHandler: @escaping (_ imageData: NSData?, _ error: NSError?) -> Void) {
        
        let url = NSURL(string: urlString)
        
        let request = NSMutableURLRequest(url: url as! URL)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
            // helper function
            func sendError(error: String) {
                let userInfo = [NSLocalizedDescriptionKey : error]
                completionHandler(nil, NSError(domain: "taskForGETMethod", code: 1, userInfo: userInfo))
            }
            
            /* GUARD: Was there an error? */
            guard error == nil else {
                sendError(error: "There was an error with your request: \(error)")
                return
            }
            
            /* GUARD: Did we get a successful 2xx response? */
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode , statusCode >= 200 && statusCode <= 299 else {
                sendError(error: "Your request returned a status code other than 2xx!")
                return
            }
            
            /* GUARD: Was there any data returned? */
            guard let data = data else {
                sendError(error: "No data was returned by the request!")
                return
            }
            
            completionHandler(data as NSData, nil)
            return
        }
        task.resume()
        
    }
}
