//
//  APOD.swift
//  NASA
//
//  Created by DONALD CHWOJKO on 8/7/17.
//  Copyright © 2017 DONALD CHWOJKO. All rights reserved.
//

import Foundation

class APOD: NSObject {
    var date: String? // YYYY-MM-DD
    var explanation: String?
    var hdUrl: String?
    var imageTitle: String?
    var url: String?
    
    init(date: String?, explanation: String?, hdUrl: String?, imageTitle: String?, url: String?) {
        self.date = date
        self.explanation = explanation
        self.hdUrl = hdUrl
        self.imageTitle = imageTitle
        self.url = url
        super.init()
    }
}
