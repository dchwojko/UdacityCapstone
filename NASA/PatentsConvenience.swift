//
//  PatentsConvenience.swift
//  NASA
//
//  Created by DONALD CHWOJKO on 8/9/17.
//  Copyright © 2017 DONALD CHWOJKO. All rights reserved.
//

import Foundation

extension NASAClient {
    func getPatents(queryString: String?, completionHandler: @escaping(_ patents: [Patent]?, _ error: NSError?)->Void) {
        var components = URLComponents()
        components.scheme = NASAClient.Constants.ApiScheme
        components.host = NASAClient.Constants.ApiHost
        components.path = NASAClient.Constants.ApiPath_Patents
        let queryItem1 = URLQueryItem(name: NASAClient.ParameterKeys.APIKEY, value: NASAClient.Constants.ApiPersonalKey)
        if (queryString != "") {
            print("DEBUG: \(queryString)")
            let queryItem2 = URLQueryItem(name: NASAClient.ParameterKeys.Patents.QUERY, value: queryString)
            components.queryItems = [queryItem1, queryItem2]
        } else {
            components.queryItems = [queryItem1]
        }
        print("DEBUG: \(components.url!)")
        self.taskForGETMethod(url: components.url) { (data, error) in
            guard error == nil else {
                print("There was an error in taskForGETMethod")
                completionHandler(nil, error)
                return
            }
            
            guard data != nil else {
                print("Data was nil")
                completionHandler(nil, nil)
                return
            }
            
            if let data = data {
                print(data)
                var patents = [Patent]()
                let patent = Patent(category: "TODO")
                patents.append(patent)
                completionHandler(patents, nil)
                return
            }
        }
    }
}
